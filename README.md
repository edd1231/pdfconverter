# My project's README
using python 3.6 and Django 2.02

I was using a virtual environment for this solution.
To setup use pip and the requirements.txt to bring in all 3rd party packages.

packages such as Cairo were a necessary addition to the WeasyPrint package I used to do the conversion from HTML to pdf.
Depending on your OS it may be necessary to use home-brew to install these packages.
https://brew.sh
from weasyprint import HTML
from django.shortcuts import render
from converter.forms import SourceForm
from django.views.generic.edit import FormView
from django.conf import settings
import os


# Create your views here.
class Index(FormView):
    media_path = settings.MEDIA_ROOT
    temp_file = os.path.join(media_path, 'tmp.html')
    pdf_file = os.path.join(media_path, 'output.pdf')
    template_name = 'index.html'
    form_class = SourceForm
    success_url = '/download'

    def form_valid(self, form):
        """
        checks form from forms.py and if valid will write to a file
        """
        print ('FORM IS VALID')
        # print (form.cleaned_data)
        pdf_source = form.cleaned_data['html_source']
        with open(Index.temp_file, 'w') as html_source:
            html_source.write(pdf_source)
        loaded_source = HTML(filename=Index.temp_file)
        loaded_source.write_pdf(Index.pdf_file)

        # print(loaded_source)
        return super().form_valid(form)

def download(request):
    return render(request, 'download.html', {})
    

from django import forms

class SourceForm(forms.Form):
    html_source = forms.CharField(widget=forms.Textarea)
